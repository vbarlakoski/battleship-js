export default class CreateGrid {
  constructor(selector = null) {
    this.output = "";
    this.selector = document.querySelector(selector);
    this.gridSize = 10;

    this.printGrid(this.gridSize, true);
    this.printGrid(this.gridSize, false);
    
  }

  /**
   * Create headers line
   * @param {param} size // Size is set in the constructor, and called while createGrid method;
   */

  createHeaders(size) {

    let headerOutput = '<h2></h2>';
    headerOutput += '<div class="header-row">';
        
    for (let i = 0; i < size + 1; i++) {
      headerOutput += `<span class="header-cell">${i}</span>`;
    }
    headerOutput += "</div>";

    return headerOutput;
  }

  /**
   * Printing the grid
   * @param {param} grid // Passes the size of grid defined in the constructor
   * @param {boolean} isEnemy // Tells if printed grid is for Enemy side, dafault set on False
   */

  printGrid(grid, isEnemy = false) {
    //Check if enemy board
    let enemyClass = `id="${isEnemy ? "enemy" : "homeland"}"`;
    //Board wrapper
    let rowStr = `<div class="grid-wrapper" ${enemyClass}>`;
    // Create the header line of the grid, and concat to output
    const headers = this.createHeaders(this.gridSize);
    rowStr += headers;

    //Opens div for the row
    rowStr += '<div class="aside-col">';
    // Start printing aside number cells
    for (let i = 1; i < grid + 1; i++) {
      rowStr += `<span class="aside-cell">${this.numToLetter(i)}</span>`;
    }
    // Closing row div
    rowStr += "</div>";
    rowStr += '<div class="grid-holder">';
    //Prints remaining of row, setting j as cell number
    for (let i = 1; i < grid + 1; i++) {
      for (let j = 1; j < grid + 1; j++) {
        let cellLetter = this.numToLetter(i);
        let cellClass = `cell-${cellLetter}-${j}`;
        cellClass += ` row-${cellLetter}`;

        if (isEnemy) {
          cellClass += ' enemy-cell';
          rowStr += `<span class="${cellClass}" data-row="${cellLetter}" data-col="${j}"></span>`;
        } else {
          rowStr += `<span class="${cellClass}" data-row="${cellLetter}" data-col="${j}"></span>`;
        }
      }
    }
    rowStr += "</div>";
    rowStr += "</div>";
    //Concats rowStr with constructor output
    this.output += rowStr;

    // Print the output in HTML
    this.selector.innerHTML = this.output;
  }

  /**
   *
   * @param {param} i //Number that will be converted to letter
   */
  numToLetter(number) {
    return (number + 9).toString(36).toUpperCase();
  }
}
