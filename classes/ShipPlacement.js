export default class ShipPlacement {

    constructor(grid) {
        this.coordX;
        this.coordY;
        this.used = [];
        this.coordLetter;
        this.gridSize = 10;
        this.name = grid;
        this.grid = document.getElementById(grid);
        this.myShips = { battleship: 5, destroyer1: 4, destroyer2: 4 };
        this.homelandShips = [{ location: ["", "", "", "", ""], hits: ["", "", "", "", ""] },
        { location: ["", "", "", ""], hits: ["", "", "", ""] },
        { location: ["", "", "", ""], hits: ["", "", "", ""] }];

        this.placeShips = this.battleShipPlacement(this.myShips);
    }

    /**
     * Mark the cells where ships are layed
     * @param {param} ships // Passes the ships object 
     */
    battleShipPlacement(ships) {
        let j = 0;
        for (const [key, value] of Object.entries(ships)) {
            this.getStartCoord(value);

            for (let i = 1; i < value + 1; i++) {
                if (this.coordY + value > this.gridSize) {
                    //if (this.name === 'homeland') {
                        this.homelandShips[j].location[i - 1] = this.coordLetter + (this.coordY - i);
                    //}
                    this.grid.getElementsByClassName(`cell-${this.coordLetter}-${this.coordY - i}`)[0].classList.add('ship');
                } else {
                   // if (this.name === 'homeland') {
                        this.homelandShips[j].location[i - 1] = this.coordLetter + (this.coordY + i);
                   // }
                    this.grid.getElementsByClassName(`cell-${this.coordLetter}-${this.coordY + i}`)[0].classList.add('ship');
                }
            }
            
            j++;
        }
    }

    /**
     *  Create random coordinate points, and assing new value for Letter row
     * @param {param} value // Pass the valaue of ship 
     */
    getRandom(value) {
        this.coordX = Math.round(Math.random(Math.floor()) * (1 - this.gridSize) + this.gridSize);
        this.coordY = Math.round(Math.random(Math.floor()) * (1 - this.gridSize) + this.gridSize);

        if (this.coordX + value > this.gridSize) {
            this.coordX = this.coordX - (this.coordX % this.gridSize);
            this.coordX === 0 ? (this.coordX = 1) : this.coordX;
        }

        if (this.coordY + value > this.gridSize) {
            this.coordY = this.coordY - (this.coordY % this.gridSize);
            this.coordY === 0 ? (this.coordY = 1) : this.coordY;
            this.coordY === 10 ? (this.coordY = 9) : this.coordY;
        }
        this.coordLetter = (this.coordX + 9).toString(36).toUpperCase();
    }


    /**
     * Defines starting point of where ship will be placed
     * @param {param} value // Pass the valaue of ship 
     */
    getStartCoord(value) {

        this.getRandom(value);

        if (this.used === 0 || !this.used.includes(this.coordLetter)) {
            this.used.push(this.coordLetter);
        } else {
            // If row is occupied, loop until empty one is found
            while (this.used.includes(this.coordLetter)) {
                this.getRandom(value);
                if (!this.used.includes(this.coordLetter)) {
                    this.used.push(this.coordLetter);
                    break;
                }
            }
        }
    }
}

// Asign prototype method, for the second call of the object 
ShipPlacement.prototype.rowHolder = function () {
    this.grid.children[2].children;
}