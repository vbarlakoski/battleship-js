**Unfortunately the game is not yet finished**



*You can [SEE DEMO](https://v.barlakoski.com/projects/battleships/) here*

---

## Programming Test: Battle ships

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## The Problem

Implement a simple game of battleships http://en.wikipedia.org/wiki/Battleship_(game) 

You must create a simple application to allow a single human player to play a one-sided game of battleships against the computer.

The program should create a 10x10 grid, and place a number of ships on the grid at random with the following sizes:
1.	1 x Battleship (5 squares)
2.	2 x Destroyers (4 squares)
Ships can touch but they must not overlap.

The application should accept input from the user in the format “A5” to signify a square to target, and feedback to the user whether the shot was success, miss, and additionally report on the sinking of any vessels.