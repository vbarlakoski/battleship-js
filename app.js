import CreateGrid from "./classes/CreateGrid.js";
import ShipPlacement from "./classes/ShipPlacement.js";

const createGrid = new CreateGrid("#ocean .ocean-wrapper");
const homelandShip = new ShipPlacement("homeland");
const enemyShip = new ShipPlacement("enemy");

const gridSize = 10;
let myTurn = true;
const input = document.querySelector('#text');
const button = document.querySelector('.btn');
const homeland = document.getElementById('homeland');
const enemy = document.getElementById('enemy');
const message = document.getElementById('message');

const validateInput = /^[A-Ja-j]([0-9]{1,2})$/;

let coordX;
let coordY;
let shot;
let count = 0;
let count2 = 0;

homeland.getElementsByTagName('h2')[0].innerText = "My Board";
enemy.getElementsByTagName('h2')[0].innerText = "Enemy's Board";


button.addEventListener('click', function(e){
    e.preventDefault;

    if(!validateInput.test(input.value.toUpperCase()) || input.value.length < 2) {
        showMessage('wrong');
    } else {
    
        let coords = input.value.toUpperCase().split(/^([A-Za-z])(.+)/);

        itsMyTurn(coords[1], coords[2]);
    }

    input.value = '';
});

function itsMyTurn(coord1, coord2) {

    let cell = enemy.getElementsByClassName(`cell-${coord1}-${coord2}`)[0];

    if(cell.classList.contains('ship')){

        if(cell.classList.contains('red') || cell.classList.contains('grey')) {
            showMessage();
        } else {
            cell.classList.add('red');
            showMessage('strike');
            count2++;
            console.log(count2);
        }
    } else {
        myTurn = false;
        button.setAttribute('disabled', 'disabled');
        input.disabled = true;
        cell.classList.add('grey');
        showMessage('miss');
        enemyStrike();
    }

    if(count2 == 13) {
        alert('You win!');
    }
    
}

function randomCoords() {
    coordY = Math.round(Math.random(Math.floor()) * (1 - gridSize) + gridSize);
    coordX = (Math.round(Math.random(Math.floor()) * (1 - gridSize) + gridSize) + 9).toString(36).toUpperCase();
    shot = homeland.getElementsByClassName(`cell-${coordX}-${coordY}`)[0];
}

function randomCoordsHit() {
    (coordY > 9) ? coordY-- : coordY++;
    shot = homeland.getElementsByClassName(`cell-${coordX}-${coordY}`)[0];
}

function showMessage(state) {

    switch(state) {
        case 'beenHit':
            message.innerText = 'You have been hit!';
        break;
        case 'strike':
            message.innerText = 'You have hitted! Its Your turn again!';
        break;
        case 'miss':
            message.innerText = 'You have missed!';
        break;
        case 'enemyMiss':
            message.innerText = 'They have missed!';
        break;
        case 'wrong':
            message.innerText = 'Check your coordinates!';
        break;
        default:
            message.innerText = 'Dont waste your torpedos!';
    }
}

function enemyStrike(hit = false) {
    if(!myTurn) {
        setTimeout(()=>{
            if(!hit) {

                randomCoords();

                if(shot.classList.contains('red')) {
                    randomCoords();
                } 

                if(shot.classList.contains('ship')) {

                    shot.classList.add('red');

                    randomCoordsHit();
                    enemyStrike.call(this, true);
                    count++
                    if(count == 13){
                        alert('Your ships are gone!')
                    }
                    showMessage('beenHit');

                } else {

                    hit = false;
                    myTurn = true;
                    showMessage('enemyMiss');
                    shot.classList.add('grey');

                    button.removeAttribute('disabled');
                    input.disabled = false;

                }

            } else {

                if(shot.classList.contains('ship')) {

                    shot.classList.add('red');

                    randomCoordsHit();
                    enemyStrike.call(this, true);

                    showMessage('beenHit');
                    count++;
                    if(count == 13){
                        alert('Your ships are gone!')
                    }

                } else {

                    myTurn = true;
                    showMessage('enemyMiss');
                    shot.classList.add('grey');

                    button.removeAttribute('disabled');
                    input.disabled = false;
                }
            }

        }, 1000);
    }
}